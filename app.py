import click
from selenium import webdriver
from lxml import html
import requests
import sys


@click.command()
@click.option('--username', prompt='Enter username', help='Bitbucket username.')
@click.option('--repo_name', prompt='Enter repository name',
                                            help='Bitbucket repository name.')

def printPRs(username, repo_name):

    #URLs for usage
    url_main = 'https://bitbucket.org/'
    url_of_PRs = url_main + username + '/' + repo_name + '/pull-requests'

    check = requests.get(url_of_PRs)

    if check.status_code != 200:
        click.echo("Please, check your inputs and try again!")
        sys.exit(0)

    browser = webdriver.Firefox()
    browser.get(url_of_PRs)

    #converting and storing in a variable an html code of webpage
    html_page = browser.page_source
    tree = html.fromstring(html_page)

    # xpath returns list of elements along the given path
    PRs_title = tree.xpath('//a[@class="pull-request-title"]/text()')
    PRs_senders_names = tree.xpath('//div[@class="pr-number-and-timestamp"]/text()')
    PRs_links = tree.xpath('//a[@class="pull-request-title"]/@href')
    browser.quit()

    if len(PRs_title) != 0:
        for i in range(len(PRs_title)):
            click.echo("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            click.echo("PR sender name:     " + PRs_senders_names[i])
            click.echo("PR title:           " + PRs_title[i])
            click.echo("PR link:            " + PRs_links[i])
            click.echo("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            click.echo()
    else:
        click.echo("You don't have any pull requests.")

if __name__ == '__main__':
    printPRs()
